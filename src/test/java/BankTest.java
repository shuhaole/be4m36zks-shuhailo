import org.junit.Rule;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.rules.ExpectedException;

import static org.junit.jupiter.api.Assertions.*;

class BankTest {
    Bank bank = new Bank();

    @Tag("fast")
    @ParameterizedTest
    @CsvFileSource(resources = "/test_deposit.csv", delimiter = ',', numLinesToSkip = 1)
    void isRegistered(String name, Integer age, Integer initialDeposit) {
        bank.registerCustomer(name, age);
        assertEquals(true, bank.isRegistered(name, age));
    }

    @Tag("fast")
    @ParameterizedTest
    @CsvFileSource(resources = "/test_deposit.csv", delimiter = ',', numLinesToSkip = 1)
    void registerCustomer(String name, Integer age) {
        bank.registerCustomer(name, age);
        assertEquals(true, bank.isRegistered(name, age));
    }

    @Tag("fast")
    @ParameterizedTest
    @CsvFileSource(resources = "/test_deposit.csv", delimiter = ',', numLinesToSkip = 1)
    void registerAccount(String name, Integer age, Integer initialDeposit) {
        bank.registerCustomer(name, age);
        bank.registerAccount(name, initialDeposit);
        assertEquals(initialDeposit, bank.getBalance(name), 0.0001);
    }

    @Tag("fast")
    @ParameterizedTest
    @CsvFileSource(resources = "/test_deposit.csv", delimiter = ',', numLinesToSkip = 1)
    void getBalance(String name, Integer age, Integer initialDeposit) {
        bank.registerCustomer(name, age);
        bank.registerAccount(name, initialDeposit);
        assertEquals(initialDeposit, bank.getBalance(name), 0.0001);
    }

    @Tag("deployment")
    @ParameterizedTest
    @CsvFileSource(resources = "/test_deposit.csv", delimiter = ',', numLinesToSkip = 1)
    void deposit(String name, Integer age, Integer initialDeposit, Integer amount, Integer expected, Boolean thrown) {
        bank.registerCustomer(name, age);
        bank.registerAccount(name, initialDeposit);
        if (thrown) {
            assertThrows(Exception.class, () -> {
                bank.deposit("John", -1);
            });
        } else {
            assertDoesNotThrow(() -> {
                bank.deposit(name, amount);
            });
        }
        assertEquals(expected, bank.getBalance(name), 0.0001);
    }

    @Tag("deployment")
    @ParameterizedTest
    @CsvFileSource(resources = "/test_withdraw.csv", delimiter = ',', numLinesToSkip = 1)
    void withdraw(String name, Integer age, Integer initialDeposit, Integer amount, Integer expected, Boolean thrown) {
        bank.registerCustomer(name, age);
        bank.registerAccount(name, initialDeposit);
        if (thrown) {
            assertThrows(Exception.class, () -> {
                bank.withdraw("John", -1);
            });
        } else {
            assertDoesNotThrow(() -> {
                bank.withdraw(name, amount);
            });
        }
        assertEquals(expected, bank.getBalance(name), 0.0001);
    }

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Tag("deployment")
    @ParameterizedTest
    @CsvFileSource(resources = "/test_transfer.csv", delimiter = ',', numLinesToSkip = 1)
    void transfer(String TransfererName, Integer TransfererAge, Integer TransfererInitial,
                  String ReceiverName, Integer ReceiverAge, Integer ReceiverInitial,
                  Integer transferAmount, Integer TransfererExpected, Integer ReceiverExpected,
                  Boolean thrown) {
        bank.registerCustomer(TransfererName, TransfererAge);
        bank.registerAccount(TransfererName, TransfererInitial);

        bank.registerCustomer(ReceiverName, ReceiverAge);
        bank.registerAccount(ReceiverName, ReceiverInitial);
        if (thrown) {
            assertThrows(Exception.class, () -> {
                bank.transfer(TransfererName, ReceiverName, transferAmount);
            });
        } else {
            assertDoesNotThrow(() -> {
                bank.transfer(TransfererName, ReceiverName, transferAmount);
            });
        }

        assertEquals(TransfererExpected, bank.getBalance(TransfererName), 0.0001);
        assertEquals(ReceiverExpected, bank.getBalance(ReceiverName), 0.0001);
    }
}

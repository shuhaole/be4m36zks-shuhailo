import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Bank {
    private final ArrayList<Customer> customers;
    private final Map<String, Double> accounts;

    public Bank() {
        customers = new ArrayList<>();
        accounts = new HashMap<>();
    }

    public boolean isRegistered(String name, int age) {
        for (Customer c : customers) {
            if (c.getName().equals(name) && c.getAge() == age) {
                return true;
            }
        }
        return false;
    }

    public void registerCustomer(String name, int age) {
        if (!isRegistered(name, age)) {
            customers.add(new Customer(name, age));
            System.out.println("Customer registered successfully!");
        } else {
            System.out.println("Customer already registered!");
        }
    }

    public void registerAccount(String name, double initialDeposit) {
        if (initialDeposit < 0){
            System.out.println("Initial deposit is lower than 0!");
        }else {
            accounts.put(name, initialDeposit);
            System.out.println("Successfully registered " + name + " with initial deposit of $" + initialDeposit + ".");
        }
    }

    public double getBalance(String name) {
        return accounts.getOrDefault(name, 0.d);
    }

    public void deposit(String name, double amount) throws Exception {
        double balance = accounts.get(name);
        if (amount < 0) {
            throw new Exception("Negative amount!");
        } else {
            balance += amount;
            accounts.put(name, balance);
            System.out.println("Successfully deposited $" + amount + " to " + name + "'s account. New balance: $" + balance + ".");
        }
    }

    public void withdraw(String name, double amount) throws Exception {
        double balance = accounts.get(name);
        if (amount > balance) {
            throw new Exception("Insufficient funds in " + name + "'s account.");
        } else if (amount < 0) {
            System.out.println("Negative amount!");
        } else {
            balance -= amount;
            accounts.put(name, balance);
            System.out.println("Successfully withdrew $" + amount + " from " + name + "'s account. New balance: $" + balance + ".");
        }
    }

    public void transfer(String nameFrom, String nameTo, double amount) throws Exception {
        withdraw(nameFrom, amount);
        deposit(nameTo, amount);
    }
}
